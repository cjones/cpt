# geo_transect.py
# -*- coding: utf-8 -*-
"""
Defines functions to interpolate to geodesic transect
@author: Chris Jones (crj6@uw.edu)
"""

from geographiclib.geodesic import Geodesic
from scipy.interpolate import RectBivariateSpline
import numpy as np

def geodesic_path(lat1, lon1, lat2, lon2):
    """Calculate geodesic path from (lat1,lon1) to (lat2, lon2)
    """
    g = Geodesic.WGS84.Inverse(lat1,lon1,lat2,lon2)
    return g
    
def get_geodesic_pts(geodes, n, lon0to360=True):
    """Get n evenly spaced interior points from geodes, 
        return [lat, lon] pairs. Also include endpoints
    """
    s12 = geodes['s12']
    pts = []
#    pts = [{'lat':geodes['lat1'],'lon':geodes['lon1']}]
    for j in range(0,n+2):
        h = Geodesic.WGS84.Direct(geodes['lat1'],geodes['lon1'],geodes['azi1'],
                                  s12*j/(n+1))
        pts.append(dict(lat=h['lat2'],lon=h['lon2']))
    # insure lon range = [0:360]
    if lon0to360:
        for p in pts:
            p['lon'] = np.mod(p['lon']+360.0,360.0)
    return pts
    
def geodesic_pts_from_coord(lat1,lon1,lat2,lon2,n):
    g = geodesic_path(lat1,lon1,lat2,lon2)
    return get_geodesic_pts(g,n)
    
def interp_to_geo(gridx,gridy,f,pts):
    """Interpolate f[gridy,gridx] to transect defined by pts[1:-1]
    Return the interpolating function
    """
    xi = np.mod(np.array([d['lon'] for d in pts[1:-1]]) + 360,360)
    yi = np.array([d['lat'] for d in pts[1:-1]])
    
    # sanitize:
    if all(np.diff(gridy)<0):
        print('warning: flipping lat')
        gridy = gridy[::-1]
        f = f[::-1,:]
    if all(np.diff(gridx)<0):
        print('warning: flipping lon')
        gridx = gridx[::-1]
        f = f[:,::-1]
    lut = RectBivariateSpline(gridy,gridx,f,kx=1,ky=1)
    return lut.ev(yi,xi)
    
def interp_to_geo3d(x,y,f,pts):
    # f assumed to have dimensions f[lev,y,x]
    nlev = f.shape[0]
    prof = np.zeros((nlev,len(pts[1:-1])),dtype=f.dtype)
    for n in range(nlev):
        prof[n,:] = interp_to_geo(x,y,np.squeeze(f[n,:,:]),pts)
    return prof