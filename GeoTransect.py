# TransectClass
# -*- coding: utf-8 -*-
"""
Class defining transect methods
Created on Mon Jun 29 21:00:01 2015

@author: cjones
"""
import numpy as np
import netCDF4 as nc
import construct_transect

class GeoTransect:
    
    def __init__(self,pts):
        self.pts = pts
        self.var = {}
    
    def add_dims(self, dim_lookup, dim_dict, time=0):
        keys = (dim_lookup['lev'],dim_lookup['lon'],dim_lookup['lat'])
        self.lev = dim_dict[keys[0]]
        self.lon = dim_dict[keys[1]]
        self.lat = dim_dict[keys[2]]        
        self.time = time
        
    def add_var(self,vnames):
        for vname in vnames:
            self.var[vname] = np.zeros((len(self.lev),len(self.pts[1:-1])))
            
    def average_2d_var(self,varin):
        lat = self.lat
        lon = self.lon
        for vname,vals in varin.items():
            self.var[vname] = construct_transect.interp_to_geo(lon,lat,vals,self.pts)            

    def average_profile_gfs(self,varin):
        """ average 3D profile, assuming GFS arrangement of dims for 
        varin[lev,lat,lon]. If 2D is passed instead
        """
        lat = self.lat
        lon = self.lon
        for vname,vals in varin.items():
            if vals.ndim==3:
                self.var[vname] = construct_transect.interp_to_geo3d(lon,lat,
                                                                     vals,self.pts)
            if vals.ndim==2:
                self.var[vname] = construct_transect.interp_to_geo(lon,lat,
                                                                   vals,self.pts)