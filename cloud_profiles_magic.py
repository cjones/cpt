# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 14:56:48 2015

@author: Chris
"""

import netCDF4 as nc
import glob
import numpy as np
import construct_transect as calcGeoPath
import GeoTransect
import matplotlib.pyplot as plt

def ncread_var3d(ncfile,ncvar):
    """ Read one or more 3d vars from single netcdf file
    """
    vout = {}
    dtup = {}
    dout = {}
    with nc.Dataset(ncfile,'r') as ncf:
        for v in ncvar:
            x = ncf.variables[v]
            vout[v] = x[:]
            dtup[v] = x.dimensions
            # add dimensions
            for dim in dtup[v]:
                if dim not in dout.keys():
                    dout[dim] = ncf.variables[dim][:]
    return vout, dtup, dout
    
def ncavg_var3d(ncfiles,ncvar,vout):
    """ Read ncvar from ncfiles; average vout in place 
    Assumes that vout was created with ncread_var3d."""
    # single file as string
    # if isinstance(ncfiles,str):
    #    vout,dtup,dout = ncread_var3d(ncfiles,ncvar)
    #    return vout,dtup,dout
    # else:
    for f in ncfiles:
        with nc.Dataset(f,'r') as ncf:
            for v in ncvar:
                vout[v] += ncf.variables[v][:]
    nf = float(len(ncfiles)+1)
    for v in ncvar:
        vout[v] = vout[v]/nf
    return vout
        
def flip_dim3(v_array,dim):
    if dim==0:
        v_array = v_array[::-1,:,:]
    elif dim==1:
        v_array = v_array[:,::-1,:]
    elif dim==2:
        v_array = v_array[:,:,::-1]
    return v_array
        
def flip_dim1(v):
    v = v[::-1]
    return v

parentdir = '/home/cjones/win/ddrive/MATLAB/comp_gfs_gfdl/nc/gfs/'

# construct transect from Long Beach to Honolulu
# Long Beach, CA
lat1 = 33.7683
lon1 = -118.1956 + 360.0

# Honolulu
lat2 = 21.3000
lon2 = -157.8167 + 360.0

npts = 10
pts = calcGeoPath.geodesic_pts_from_coord(lat1,lon1,lat2,lon2,npts)

dims_to_load = {'lat': 'lat_2', 'lon':'lon_2','lev':'lv_ISBL9'}
vars_to_load = ['CLWMR_2_ISBL']

ncfiles = glob.glob(parentdir+"*.nc")

nc1 = nc.Dataset(ncfiles[0],'r')
lev = nc1.variables[dims_to_load['lev']][:]
clwmr = nc1.variables[vars_to_load[0]]
# nc1.close()

v1, d1, d2 = ncread_var3d(ncfiles[0],vars_to_load)
v3 = v1.copy()
v2 = ncavg_var3d(ncfiles[1:-1],vars_to_load,v1)
v4 = v2.copy()

# flip lats as needed:
for key,v in d2.items():
    # reverse the dimension
    if all(np.diff(v)<0):
        print('flipping dimension {0}'.format(key))
        d2[key] = v[::-1]
        # identify which index of v2 needs to be swapped:
        for var,tup in d1.items():
            ix = tup.index(key)
            print('flipping index {0} for variable {1}'.format(ix,var))
            v2[var] = flip_dim3(v2[var],ix)

Trans = GeoTransect.GeoTransect(pts)
Trans.add_dims(lev)
Trans.add_var(vars_to_load) 
Trans.average_profile_gfs(v2,d2)

plt.plot(Trans.var['CLWMR_2_ISBL'],Trans.lev)
plt.gca().invert_yaxis()
plt.show()

# Trans.ncread_vars(ncfiles,dims_to_load)

# ORIGINAL VERSION FOR COMPARISON
lats = []
lons= []
lvs = []
clwmr = []
for f in glob.glob(parentdir+"*.nc"):
    print(f)
    with nc.Dataset(f,'r') as ncfile:
        clwmr.append(ncfile.variables['CLWMR_2_ISBL'][:])
        lats.append(ncfile.variables['lat_2'][:])
        lons.append(ncfile.variables['lon_2'][:])
        lvs.append(ncfile.variables['lv_ISBL9'][:])
        
# horizontal average
cld_prof = np.sum(clwmr,axis=0)/len(clwmr)


g = calcGeoPath.geodesic_path(lat1,lon1,lat2,lon2)
pts = calcGeoPath.get_geodesic_pts(g,npts)

x = lons[0]
y = lats[0]
nlat = len(y)
nlon = len(x)
nlev = len(lvs[0])
grid_lon, grid_lat = np.meshgrid(x,y)

# im1 = plt.contourf(grid_lon,grid_lat,cld_prof[3,:,:])
# plt.xlim(200,250)
# plt.ylim(10,40)
# cb = plt.colorbar(im1)
# plt.show()

if all(np.diff(y)<0):
    # reverse order:
    y = y[::-1]
    cld_prof = cld_prof[:,::-1,:]
    # sanity check
    # grid_lon, grid_lat = np.meshgrid(x,y)
    #im2 = plt.contourf(grid_lon,grid_lat,cld_prof[3,:,:])
    #plt.xlim(200,250)
    #plt.ylim(10,40)
    #cb = plt.colorbar(im2)
    #plt.show()

transect_profile = np.zeros((nlev,npts),dtype=x.dtype)
for n in range(len(cld_prof)):
    transect_profile[n,:] = calcGeoPath.interp_to_geo(x,y,np.squeeze(cld_prof[n,:,:]),pts)

plt.plot(transect_profile,lvs[0])
plt.gca().invert_yaxis()
plt.show()
#
#ncfile = nc.Dataset(f,'r')
#clw = ncfile.variables['CLWMR_2_ISBL']
