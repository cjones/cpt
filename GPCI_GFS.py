# GPCI Transect
# -*- coding: utf-8 -*-
"""
Construct monthly mean forecasts from GFS model along GPCI Transect:

Created on Thu Jul  2 22:57:34 2015

@author: cjones
"""

import netCDF4 as nc
# import glob
import os
import re
import numpy as np
import construct_transect as calcGeoPath
import GeoTransect
import matplotlib.pyplot as plt


def gpci_transect():
    # S12
    lat1 = 35
    lon1 = -125
    
    # S1
    lat2 = -1
    lon2 = -173
    
    pts = []
    for n in range(-1,14):
        pts.append({'lon':lon2+4*n,'lat':lat2+3*n})
    return pts
    
def ncread_var3d(ncfile,ncvar):
    """ Read one or more 3d vars from single netcdf file
    """
    vout = {}
    dtup = {}
    dout = {}
    with nc.Dataset(ncfile,'r') as ncf:
        for v in ncvar:
            x = ncf.variables[v]
            vout[v] = x[:]
            dtup[v] = x.dimensions
            # add dimensions
            for dim in dtup[v]:
                if dim not in dout.keys():
                    dout[dim] = ncf.variables[dim][:]
    return vout, dtup, dout
    
def ncavg_var3d(ncfiles,ncvar):
    """ Read ncvar from ncfiles; average vout in place 
    Assumes that vout was created with ncread_var3d."""
    # single file as string converted to list:
    if isinstance(ncfiles,str):
        ncfiles = [ncfiles]
    nf = float(len(ncfiles))
    vout,dtup,dout = ncread_var3d(ncfiles[0],ncvar)
    for f in ncfiles[1:]:
        with nc.Dataset(f,'r') as ncf:
            for v in ncvar:
                vout[v] += ncf.variables[v][:]
    # average
    for v in ncvar:
        vout[v] = vout[v]/nf
    return vout, dtup, dout
        
def flip_dim3(v_array,dim):
    if dim==0:
        v_array = v_array[::-1,:,:]
    elif dim==1:
        v_array = v_array[:,::-1,:]
    elif dim==2:
        v_array = v_array[:,:,::-1]
    return v_array

def flip_dim(v_array,dim):
    """ Flips array along dimension dim. Works for arbitrarily sized arrays
    (and thus is an improvement over flip_dim3)
    """
    reversed_arr = np.swapaxes(np.swapaxes(v_array, 0, dim)[::-1], 0, dim)
    return reversed_arr

if __name__=="__main__":
    pts = gpci_transect()
    

    # July 2013 monthly composite of 0-24hr forecasts
    gfs_dir = '/home/cjones/atmos_gfs_gfdl/gfs/2.5/'
    pattern_1day = re.compile(r'201307(\d{2})\.gfs\.t00z\.pgrbf(03|06|09|12|15|18|21|24)\.2p5deg\.nc$')
    pattern_6h = re.compile(r'201307(\d{2})\.gfs\.t00z\.pgrbf(06|12|18|24)\.2p5deg\.nc$')
    pattern_2day = re.compile(r'201307(\d{2})\.gfs\.t00z\.pgrbf(27|30|33|36|39|42|45|48)\.2p5deg\.nc$')

    ncfiles = []
    ncfiles_6h = []
    for dir1, subdir1, files in os.walk(gfs_dir):
        for f in files:
            if pattern_1day.search(f) is not None:
                ncfiles.append(dir1+f)
            if pattern_6h.search(f) is not None:
                ncfiles_6h.append(dir1+f)
            
    print('Constructing average from {0} netcdf files'.format(len(ncfiles)))
    
    dims_to_load = {'lat': 'lat_2', 'lon':'lon_2','lev':'lv_ISBL9'}
    vars_to_load = ['TMP_2_ISBL','CLWMR_2_ISBL','R_H_2_ISBL','V_VEL_2_ISBL']
    vars_6h = ['PRATE_2_SFC_ave6h','USWRF_2_NTAT_ave6h','ULWRF_2_NTAT_ave6h',
               'SPF_H_2_HTGL']
    
    v2, d1, d2 = ncavg_var3d(ncfiles[0:8],vars_to_load)
    v3, d3, d4 = ncavg_var3d(ncfiles_6h[0:8],vars_6h)
    
    v2.update(v3)
    d1.update(d3)
    d2.update(d4)

    # flip lats as needed:
    for key,v in d2.items():
        # reverse the dimension
        if all(np.diff(v)<0):
            print('flipping dimension {0}'.format(key))
            d2[key] = v[::-1]
            # identify which index of v2 needs to be swapped:
            for var,tup in d1.items():
                ix = tup.index(key)
                print('flipping index {0} for variable {1}'.format(ix,var))
                v2[var] = flip_dim(v2[var],ix)
    
    Trans = GeoTransect.GeoTransect(pts)
    Trans.add_dims(dims_to_load,d2)
    Trans.add_var(vars_to_load) 
    Trans.average_profile_gfs(v2)
    
    lons = np.array([x['lon'] for x in pts[1:-1]])
    
    for v,c in Trans.var.items():
        nd = len(d1[v])
        # 2D: make line plot
        if nd==2:
            plt.plot(lons,c)
        # 3D: make pcolor plot
        elif nd==3:
            z = d2[d1[v][0]]
            plt.pcolor(lons,z,c)
            plt.colorbar()
            plt.gca().invert_yaxis()
            
        plt.title(v)
        plt.show()