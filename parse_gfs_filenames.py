# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 12:39:06 2015

@author: cjones
"""

#import netCDF4 as nc
# import glob
import os
import re
#import numpy as np
#import construct_transect as calcGeoPath
#import GeoTransect
#import matplotlib.pyplot as plt

# pattern:
# '20130723.gfs.t00z.pgrbf78.2p5deg.nc'
# 'yyyymmdd.gfs.t00z.pgrbf[hh].2p5deg.nc' where hh is forecast hour
# regexp is easiest?
gfs_dir = '/home/cjones/atmos_gfs_gfdl/gfs/2.5/'
pattern_1day = re.compile(r'201307(\d{2})\.gfs\.t00z\.pgrbf(03|06|09|12|15|18|21|24)\.2p5deg\.nc$')
pattern_2day = re.compile(r'201307(\d{2})\.gfs\.t00z\.pgrbf(27|30|33|36|39|42|45|48)\.2p5deg\.nc$')

nclist = []
for dir1, subdir1, files in os.walk(gfs_dir):
    for f in files:
        out = pattern_1day.search(f)
        if out is not None:
            nclist.append(dir1+f)
