# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 16:50:12 2015

@author: cjones
"""

import netCDF4 as nc
import GPCI_GFS as gpci
import os
import re
import numpy as np

def slice_range(v_array,vmin,vmax):
    """ Construct simple slicing for 1D array
    """
    sel = ((v_array>=vmin) & (v_array <= vmax)).nonzero()[0]
    return slice(sel[0],sel[-1]+1)

def slice_xt(v_array,tup):
    """ Construct simple slicing for arbitrary length array indexing
    tup = ((i1,slice(i1)),(i2,slice(i2)),...)
    """
    

def ncread_var4d(ncfile,ncvar):
    """ Read one or more vars from single netcdf file
    """
    vout = {}
    dtup = {}
    dout = {}
    with nc.Dataset(ncfile,'r') as ncf:
        for v in ncvar:
            x = ncf.variables[v]
            vout[v] = x[:]
            dtup[v] = x.dimensions
            # add dimensions
            for dim in dtup[v]:
                if dim not in dout.keys():
                    dout[dim] = ncf.variables[dim][:]
    return vout, dtup, dout
    
def nctime_avg(ncfile,ncvar,tmin=-np.Inf,tmax=np.Inf,tvar='time',latmin=-10,
               latmax=45,lonmin=175,lonmax=250):
    """ Average one or more vars from single netcdf file over the 
    time dimension for time in [tmin:timax]"""
    vout = {}
    dtup = {}
    dout = {}
    with nc.Dataset(ncfile,'r') as ncf:
        for v in ncvar:
            x = ncf.variables[v]
            dtup[v] = x.dimensions
            # add dimensions
            for ix, dim in enumerate(dtup[v]):
                if dim not in dout.keys():
                    dout[dim] = ncf.variables[dim][:]
                    # only need to refine selection 1x
                    if dim==tvar:
                        sel = (dout[dim]>=tmin) & (dout[dim]<=tmax)
                    elif dim=='lat':
                        sel_lat = (dout[dim]>=latmin) & (dout[dim]<=latmax)
                    elif dim=='lon':
                        sel_lon = (dout[dim]>=lonmin) & (dout[dim]<=lonmax)
            # note: in future I'll generalize this, but for now I know that
            # time is always first dimension, so this is easy -- may break bad
            # vout[v] = np.average(x[sel],axis=0)
            vout[v] = np.average(x[sel,:,sel_lat,sel_lon],axis=0)
    return vout, dtup, dout
    
def ncselect(ncfile,ncvar,tmin=-np.Inf,tmax=np.Inf,tvar='time',latmin=-10,
               latmax=45,lonmin=175,lonmax=250):
    """ Average one or more vars from single netcdf file over the 
    time dimension for time in [tmin:timax]"""
    vout = {}
    dtup = {}
    dout = {}
    dsel = {}
    with nc.Dataset(ncfile,'r') as ncf:
        for v in ncvar:
            x = ncf.variables[v]
            dtup[v] = x.dimensions
            # add dimensions
            for ix, dim in enumerate(dtup[v]):
                if dim not in dout.keys():
                    dout[dim] = ncf.variables[dim][:]
                    # only need to refine selection 1x
                    if dim==tvar:
                        dsel[dim]=slice_range(dout[dim],tmin,tmax)
                    elif dim=='lat':
                        dsel[dim]=slice_range(dout[dim],latmin,latmax)
                    elif dim=='lon':
                        dsel[dim]=slice_range(dout[dim],lonmin,lonmax)
                    else:
                        dsel[dim]=slice(None)
            # note: in future I'll generalize this, but for now I know that
            # time is always first dimension, so this is easy -- may break bad
            # vout[v] = np.average(x[sel],axis=0)
            
            # subset
            sel = tuple(dsel[dim] for dim in x.dimensions)
            vout[v] = np.average(x[sel],axis=dtup[v].index(tvar))
    return vout, dtup, dout    

def ncavg_multi(ncfiles,ncvar,tmin=-np.Inf,tmax=np.Inf,tvar='time',latmin=-10,
               latmax=45,lonmin=175,lonmax=250):
    """ Read ncvar from ncfiles; average vout in place 
    Assumes that vout was created with ncread_var3d."""
    # single file as string converted to list:
    if isinstance(ncfiles,str):
        ncfiles = [ncfiles]
    nf = float(len(ncfiles))
#    vout,dtup,dout = nctime_avg(ncfiles[0],ncvar,tmin,tmax,tvar,latmin,
#                                latmax,lonmin,lonmax)
    vout,dtup,dout = ncselect(ncfiles[0],ncvar,tmin,tmax,tvar,latmin,
                                latmax,lonmin,lonmax)
    for f in ncfiles[1:]:
#        vtmp,dtmp,dtmp2 = nctime_avg(f,ncvar,tmin,tmax,tvar,latmin,
#                                latmax,lonmin,lonmax)
        vtmp,dtmp,dtmp2 = ncselect(f,ncvar,tmin,tmax,tvar,latmin,
                                latmax,lonmin,lonmax)
        for v in ncvar:
            vout[v] += vtmp[v]
    # average assuming equal weights to all days
    for v in ncvar:
        vout[v] = vout[v]/nf
    return vout, dtup, dout

# File pattern:
# 20130701_atmos_24xdaily_L13_2p5X2p5.nc

if __name__=="__main__":
    pts = gpci.gpci_transect()
    
    # identify variables to load
    dims_to_load = {'lat': 'lat', 'lon': 'lon','lev': 'level','time': 'time'}
    vars4d = ['cld_amt','liq_wat','ice_wat','omega','rh','sphum','temp']
    vars3d = ['LWP','IWP','precip','uw_precip']
    vars8x_daily = ['TOA_Down_SW','TOA_Up_SW','TOA_OLR',
                    'TOA_Down_SW_clr','TOA_Up_SW_clr','TOA_OLR_clr']
    
    # July 2013 monthly composite of 0-24hr forecasts
    # get list of netcdf files
    # file pattern: 201307(dd)_atmos_(8x|24x)daily_L13_2p5X2p5.nc
    
    gfdl_dir = os.getenv('HOME')+'/eos4/gfs_gfdl_obs/gfdl/am4/'
    pattern_24x = re.compile(r'201307(\d{2})_atmos_24xdaily_L13_2p5X2p5\.nc$')
    pattern_8x = re.compile(r'201307(\d{2})_atmos_8xdaily_L13_2p5X2p5\.nc$')
    
    ncfiles24x = []
    ncfiles8x = []
    for dir1, subdir1, files in os.walk(gfdl_dir):
        for f in files:
            if pattern_24x.search(f) is not None:
                ncfiles24x.append(dir1+f)
            if pattern_8x.search(f) is not None:
                ncfiles8x.append(dir1+f)
            
    # load and average variables
    v4d, d1, d2 = ncavg_multi(ncfiles24x[0],vars4d,tmax=1.0)
    for ncf in ncfiles24x[1:10]:
        vtmp, d1, d2 = ncavg_multi(ncf,vars4d,tmax=1.0)
        
        # for v in v4d.keys():
        #    v4d[v] += vtmp[v]
    # no need to flip junk either.
    
    # make plots
